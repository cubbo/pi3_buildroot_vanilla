FROM phoggy/pi3_buildroot_vanilla
WORKDIR /
ADD br_external /br_external

WORKDIR /buildroot
RUN make BR2_EXTERNAL=../br_external clean
RUN make defconfig BR2_DEFCONFIG=../br_external/configs/pi3_br_vanilla_defconfig
