################################################################################
#
# python-rpimotorlib
#
################################################################################

PYTHON_RPIMOTORLIB_VERSION = 2.6
PYTHON_RPIMOTORLIB_SOURCE = $(PYTHON_RPIMOTORLIB_VERSION).tar.gz
PYTHON_RPIMOTORLIB_SITE = https://github.com/gavinlyonsrepo/RpiMotorLib/archive
PYTHON_RPIMOTORLIB_SETUP_TYPE = setuptools
PYTHON_RPIMOTORLIB_LICENSE = MIT

$(eval $(python-package))
