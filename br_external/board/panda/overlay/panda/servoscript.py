import RPi.GPIO as GPIO
import time
from RpiMotorLib import RpiMotorLib

#GPIO Pins
button=25
direction=26
step=19

servo = RpiMotorLib.A3967EasyNema(direction, step, (20, 21))
servo.motor_move(.0010, 1600, True, False, "1/8", .05)

GPIO.cleanup()
