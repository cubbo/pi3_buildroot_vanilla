import time
from rpi_ws281x import Color, PixelStrip, ws

import RPi.GPIO as GPIO
from RpiMotorLib import RpiMotorLib

#Motor config
direction=26
step=19

#led config
LED_COUNT = 30
LED_PIN = 18
LED_FREQ_HZ = 800000
LED_DMA = 10
LED_MAX_BRIGHT = 255
LED_STRIP = ws.SK6812_STRIP

#init
strip = PixelStrip(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, False, LED_MAX_BRIGHT, 0, LED_STRIP)
strip.begin()
servo = RpiMotorLib.A3967EasyNema(direction, step, (20,21))

#fade in leds
for i in range(LED_MAX_BRIGHT):
  for j in range(strip.numPixels()):
    strip.setPixelColor(j,Color(i,i,i))
  strip.show()
  time.sleep(0.005)

#spin servo
servo.motor_move(0.001, 400, True, False, "1/8", 0.05)
servo.motor_move(0.001, 400, False, False, "1/8", 0.05)

GPIO.cleanup()
